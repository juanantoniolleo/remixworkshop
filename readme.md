## Workshop de Smart Contracts con Remix - Juan Antonio Lleó

### INTRODUCCION:
Se trata de un taller eminentemente práctico, que tiene como objetivo una primera toma de contacto con el análisis y el desarrollo de los contratos inteligentes (Smart Contracts) sobre Ethereum, usando el lenguaje Solidity.
Mediante la herramienta en línea Remix se va a ver cómo interactuar con distintos ejemplos de contratos inteligentes, cómo editarlos y modificarlos, cómo añadir nuevas funciones, etc.
Asimismo se va a ver de que manera compilarlos y desplegarlos, tanto en local como en distintas testnets y en la mainnet.

### REQUERIMIENTOS:
Para poder participar en el taller, es aconsejable cubrir los siguientes requerimientos:

- Metamask instalado en tu navegador:
https://metamask.io/

- Saldo en la testnet de Goerli. Para conseguirlo, se puede usar la siguiente dirección (faucet):
https://goerli-faucet.slock.it/

- Probar que funciona el Remix en tu navegador:
https://remix.ethereum.org/

### MEETUP:
https://www.meetup.com/es-ES/Crypto-Plaza/events/274552176/

### ENLACES DE INTERES:

https://ethereum.org/en/developers/docs/smart-contracts/

#### ERC20:
https://ethereum.org/en/developers/docs/standards/tokens/erc-20/
https://eips.ethereum.org/EIPS/eip-20

Example implementations are available at
- OpenZeppelin implementation:
https://github.com/OpenZeppelin/openzeppelin-solidity/blob/9b3710465583284b8c4c5d2245749246bb2e0094/contracts/token/ERC20/ERC20.sol

- ConsenSys implementation:
https://github.com/ConsenSys/Tokens/blob/fdf687c69d998266a95f15216b1955a4965a0a6d/contracts/eip20/EIP20.sol

Implements EIP20 token standard:
- EN ESTA PAGINA SE INCLUYEN LOS COMENTARIOS A LOS METODOS IMPLEMENTADOS EN UN ERC20:
https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md


#### ETHEREUM STUDIO:
https://studio.ethereum.org/

INCLUYE VARIAS TEMPLATES PARA APRENDER:
- Hello World: A Hello World style starter project. Deploys a smart contract with a message, and renders it in the frontend. You can change the message using the interact panel!
- Token: A starter dapp that defines a basic token you can create and send to others.
- CryptoPizza NFT: A game built on top of the ERC721 standard for creating unique collectible tokens.
- Empty Project: An empty boilerplate which can help you to get started quickly.


### OTROS ENLACES:

https://gitlab.com/juanantoniolleo/the_ground/-/tree/master/The_Wall







